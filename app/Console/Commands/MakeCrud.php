<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeCrud extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:crud {name} {--model} {--migration}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permet de générer un crud directement';

    public array $fields = [];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $fields = $this->ask('Quels seront les champs présents dans votre formulaire ?');
        $this->fields = (explode(',',$fields));

        $this->line('');
        $this->info('Génération du Controller...');
        $this->line('');

        $this->generateController();

        $this->line('');
        $this->info('Génération du Model...');
        $this->line('');

        $this->generateModel();

        $this->call('make:policy', [

            'name' => ucfirst($this->argument('name')). 'Policy',
            '--model' => $this->argument('name')
        ]);

        $this->generatePolicy();

        $this->line('');
        $this->info('Génération des templates...');
        $this->line('');
        $this->generateLiveWireTable();
        $this->generateIndexTemplate();
        $this->generateFormTemplate();

        $this->line('');
        $this->info('Le CRUD a été généré avec succès !');

        /**
         * Si l'option migrate est activée, on exécute la migration
         */
        if( $this->option('migration') ) {
            $this->call('make:migration', [
                'name' => 'create_' . Str::snake($this->argument('name')) . 's' . '_table',
                '--create' => Str::snake($this->argument('name')) . 's',
            ]);
        }

        return 0;
    }

    /**
     * Génère le Model
     */
    public function generateModel()
    {
        $fillable = "";
        foreach( $this->fields as $field ) {
            $fillable .= "'" . $field . "' ," . "\n";
        }

        /**
         * On parse le Presenter
         */
        $presenterTemplate = str_replace([
            '{{ namespace }}',
            '{{ class }}'
        ], [
            'App\Models\Presenters',
            ucfirst($this->argument('name'))
        ], $this->getStub('Presenter'));

        /**
         * On parse le Model
         */
        $modelTemplate = str_replace(
            [
                '{{ namespace }}',
                '{{ fillable }}',
                '{{ class }}'
            ],
            [
                'App\Models',
                $fillable,
                ucfirst($this->argument('name'))
            ],
            $this->getStub('Model')
        );

        //  On sauvegarde notre Model
        $modelName = ucfirst($this->argument('name')) . '.php';
        $this->saveStubFile(app_path('Models/' . $modelName), $modelTemplate);

        //  On sauvegarde notre Presenter
        $presenterName = ucfirst($this->argument('name')) . 'Presenter.php';
        $this->saveStubFile(app_path('Models/Presenters/' . $presenterName), $presenterTemplate);
    }

    /**
     * Génère le Controller
     */
    public function generateController()
    {
        $fieldsRules = "";
        foreach( $this->fields as $field ) {
            $fieldsRules .= "'" . $field . "' => 'required'," . "\n";
        }

        //  On parse le Controller
        $controllerTemplate = str_replace([
            '{{ namespace }}',
            '{{ model }}',
            '{{ modelVariable }}',
            '{{ fieldRules }}'
        ], [
            'App\Http\Controllers\Admin',
            ucfirst($this->argument('name')),
            strtolower($this->argument('name')),
            $fieldsRules
        ], $this->getStub('Controller'));

        //  On sauvegarde notre Controller
        $controllerName = ucfirst($this->argument('name')) . 'sController.php'; // => 'UsersController';
        $this->saveStubFile(app_path('Http/Controllers/Admin/' . $controllerName), $controllerTemplate);
    }

    public function generateIndexTemplate()
    {
        $indexTemplate = str_replace([
            '{{ pluralModelName }}',
            '{{ modelPath }}',
            '{{ lowerModelName }}'
        ], [
            //  On récupère la clé de langue déjà présente dans le fichier de langue
            "__(" . ucfirst($this->argument('name')) . 's' . ")",
            'App\Models\\' . ucfirst($this->argument('name')),
            strtolower($this->argument('name'))
        ], $this->getStub('index.blade'));

        if( !file_exists(resource_path('/views/admin/' . Str::snake($this->argument('name')) . 's')) ) {
            mkdir(resource_path('/views/admin/' . Str::snake($this->argument('name')) . 's'));
        }

        $this->saveStubFile(resource_path('/views/admin/' . Str::snake($this->argument('name')) . 's/index.blade.php'), $indexTemplate);
    }




    public function generateFormTemplate()
    {
        $formTemplate = str_replace([
            '{{ modelVariable }}',
            '{{ pluralModelName }}'
        ], [
            Str::camel($this->argument('name')),
            Str::plural(Str::lower($this->argument('name')))
        ], $this->getStub('form.blade'));

        if( !file_exists(resource_path('/views/admin/' . Str::snake($this->argument('name')) . 's')) ) {
            mkdir(resource_path('/views/admin/' . Str::snake($this->argument('name')) . 's'));
        }

        $this->saveStubFile(resource_path('/views/admin/' . Str::snake($this->argument('name')) . 's/form.blade.php'), $formTemplate);
    }

    public function generateLiveWireTable()
    {
        $tableTemplate = str_replace([
            '{{ modelName }}',
            '{{ routeName }}'
        ], [
            ucfirst(Str::camel($this->argument('name'))),
            Str::plural(Str::lower($this->argument('name')))
        ], $this->getStub('Table'));

        $this->saveStubFile(app_path('/Http/Livewire/' . ucfirst(Str::snake($this->argument('name'))) . 'Table.php'), $tableTemplate);
    }

    /**
     * Récupère le fichier concerné
     *
     * @param $type
     * @return false|string
     */
    protected function getStub($type)
    {
        return file_get_contents(resource_path("stubs/$type.stub"));
    }

    protected function saveStubFile($file, $data)
    {
        if( !file_exists($file) ) {
            file_put_contents($file, $data);
        }
    }
}
