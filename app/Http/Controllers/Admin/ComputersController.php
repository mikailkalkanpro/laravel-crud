<?php
namespace App\Http\Controllers\Admin;

use \App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use \App\Models\Computer;

class ComputersController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Computer::class, 'computer');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        
        return view('admin.computers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $computer = new Computer();
        $brands = Brand::select('id', 'name')->pluck('name', 'id');
        return view('admin.computers.form', compact('computer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'comment' => 'required',
            'stock_type' => 'required',
            'is_available' => 'required',
            'image' => 'required',

        ]);
        $computer = Computer::create($request->all());
        return redirect()->route('admin.computer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Computer $computer
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Computer $computer)
    {
        $brands = Brand::select('id', 'name')->pluck('name', 'id');
        $computer = Computer::findOrFail($computer);
        return view('admin.computers.form', compact('computer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required',
            'slug' => 'required',
            'comment' => 'required',
            'stock_type' => 'required',
            'is_available' => 'required',
            'image' => 'required',
        ]);
        $computer = Computer::findOrFail($id);
        $computer->update($request->all());
        return redirect()->route('admin.computer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $computer = Computer::findOrFail($id);
        $computer->delete();
        return redirect()->route('admin.computers.index');
    }
}
