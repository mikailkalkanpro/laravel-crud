<?php
namespace App\Http\Controllers\Admin;

use \App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Models\Repair;

class RepairsController extends Controller
{
     public function __construct()
     {
        $this->authorizeResource(Repair::class, 'repair');
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $repairs = Repair::all();
        return view('admin.repairs.index', compact('repairs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $repair = new Repair();
        return view('admin.repairs.form', compact('repair'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'repairs' => 'required',

        ]);
        Repair::create($request->all());
        return redirect()->route('admin.repairs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $repair = Repair::findOrFail($id);
        return view('admin.repairs.form', compact('repair'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'repairs' => 'required',

        ]);
        $repair = Repair::findOrFail($id);
        $repair->update($request->all());
        return redirect()->route('admin.repairs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $repair = Repair::findOrFail($id);
        $repair->delete();
        return redirect()->route('admin.repairs.index');
    }
}
