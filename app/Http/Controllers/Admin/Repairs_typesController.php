<?php
namespace App\Http\Controllers\Admin;

use \App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Models\Repairs_type;

class Repairs_typesController extends Controller
{
     public function __construct()
     {
        $this->authorizeResource(Repairs_type::class, 'repairs_type');
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $repairs_types = Repairs_type::all();
        return view('admin.repairs_types.index', compact('repairs_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $repairs_type = new Repairs_type();
        return view('admin.repairs_types.form', compact('repairs_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'repairs_types' => 'required',

        ]);
        Repairs_type::create($request->all());
        return redirect()->route('admin.repairs_types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $repairs_type = Repairs_type::findOrFail($id);
        return view('admin.repairs_types.form', compact('repairs_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'repairs_types' => 'required',

        ]);
        $repairs_type = Repairs_type::findOrFail($id);
        $repairs_type->update($request->all());
        return redirect()->route('admin.repairs_types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $repairs_type = Repairs_type::findOrFail($id);
        $repairs_type->delete();
        return redirect()->route('admin.repairs_types.index');
    }
}
