<?php
namespace App\Http\Livewire;

use App\Models\Brand;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class BrandTable extends LivewireDatatable
{

    public $hideable = 'select';

    public function builder()
    {
        return Brand::query();
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->label('ID'),

            Column::name('name')->label('Nom')->searchable(),
            Column::name('slug')->label('Slug')->searchable(),

            Column::callback(['id'], function ($id) {
                return view('components.table-actions', ['id' => $id, 'routeName' => 'brands']);
            })->unsortable()
        ];
    }
}
