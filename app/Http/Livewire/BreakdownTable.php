<?php
namespace App\Http\Livewire;

use App\Models\Breakdown;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class BreakdownTable extends LivewireDatatable
{
    public $model = Breakdown::class;

    public $hideable = 'select';

    public function columns()
    {
        return [
            NumberColumn::name('id')->label('ID'),

            Column::name('name')->label('Nom')->searchable(),

            Column::callback(['id'], function ($id) {
                return view('components.table-actions', ['id' => $id, 'routeName' => 'breakdowns']);
            })->unsortable()
        ];
    }
}
