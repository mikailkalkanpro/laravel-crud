<?php
namespace App\Http\Livewire;

use App\Models\Computer;
use App\Models\Brand;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class ComputerTable extends LivewireDatatable
{
    public $hideable = 'select';

    public function builder()
    {
        return Computer::query()
            ->join('brands', 'brands.id', '=', 'computers.brand_id'); // Join the brands table
    }

    public function columns()
    {
        return [


            Column::callback(['image'], function ($image) {
                return '<img src="' . $image . '" alt="Brand Image" width="100px" height="100px">';
            })->label('Image')->unsortable(),
            Column::name('name')->label('Nom')->searchable(),
            Column::name('case')->label('Boitier')->searchable(),
            Column::name('processor')->label('Processeur')->searchable(),
            Column::name('ram')->label('Ram')->searchable(),
            Column::name('graphic_card')->label('graphic_card')->searchable(),
            Column::name('stokage')->label('Stockage')->searchable(),
            Column::name('brands.name')->label('Marque')->searchable(), // Use brands.name
            Column::name('price')->label('Prix')->searchable(),



            Column::callback(['is_available'], function ($is_available) {
                return $is_available ? 'Vrai' : 'Faux';
            })->label('Disponible')->unsortable(),

            Column::callback(['id'], function ($id) {
                return view('components.table-actions', ['id' => $id, 'routeName' => 'computers']);
            })->unsortable()
        ];
    }
}