<?php
namespace App\Http\Livewire;

use App\Models\Product;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class ProductTable extends LivewireDatatable
{
    public $model = Product::class;

    public function columns()
    {
        return [
            NumberColumn::name('id')->label('ID'),

            Column::name('name')->label('Nom')->searchable(),
            Column::name('detail')->label('Detail')->searchable(),

            Column::name('Actions')->callback(['id'], function ($id) {
                return view('components.table-actions', ['id' => $id, 'routeName' => 'products']);
            })->unsortable()
        ];
    }
}