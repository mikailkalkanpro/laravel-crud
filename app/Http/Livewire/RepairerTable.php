<?php
namespace App\Http\Livewire;

use App\Models\Repairer;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class RepairerTable extends LivewireDatatable
{
    public $model = Repairer::class;

    public $hideable = 'select';

    public function columns()
    {
        return [
            NumberColumn::name('id')->label('ID'),

            Column::name('name')->label('Nom')->searchable(),

            Column::callback(['id'], function ($id) {
                return view('components.table-actions', ['id' => $id, 'routeName' => 'repairers']);
            })->unsortable()
        ];
    }
}
