<?php
namespace App\Http\Livewire;

use App\Models\Role;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class RoleTable extends LivewireDatatable
{
    public $model = Role::class;

    public $hideable = 'select';

    public function columns()
    {
        return [
            NumberColumn::name('id')->label('ID'),

            Column::name('name')->label('Nom')->searchable(),
            Column::name('slug')->label('Slug')->searchable(),

            NumberColumn::name('users.id:sum')->label('Utilisateurs')->unsortable(),

            Column::name('Actions')->callback(['id'], function ($id) {
                return view('components.table-actions', ['id' => $id, 'routeName' => 'roles']);
            })->unsortable()
        ];
    }
}
