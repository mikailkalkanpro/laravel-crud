<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Presenters\ComponentPresenter;

class Component extends Model
{
    use HasFactory;

    public $fillable = [
        'components' ,
        'name' ,
        'serial_number' ,
        'comment' ,
    ];
}
