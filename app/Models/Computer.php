<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Computer extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'slug',
        'comment',
        'case',
        'processor',
        'ram',
        'graphic_card',
        'stokage',
        'is_available',
        'image',
        'price',
    ];

    // Additional model logic and relationships can be defined here
}
