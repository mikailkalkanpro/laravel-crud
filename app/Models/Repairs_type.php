<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Presenters\Repairs_typePresenter;

class Repairs_type extends Model
{
    use HasFactory;

    public $fillable = [
        'repairs_types' ,

    ];
}
