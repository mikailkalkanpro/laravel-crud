<?php
namespace App\Presenters;

trait UserPresenter
{
    public function getTableFields()
    {
        return [
            'id',
            'name',
            'slug',
        ];
    }

    public function getFormFields()
    {
        return [
            'name'      =>  'text',
            'slug'    =>  'text',
        ];
    }

}
