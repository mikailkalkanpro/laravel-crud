<?php
namespace App\Presenters;

trait ComputerPresenter
{
    public function getTableFields()
    {
        return [
            'id',
            'name',
            'slug',
            'comment',
            'stock_type',
            'is_available',
            'image',
        ];
    }

    public function getFormFields()
    {
        return [
            'name'      =>  'text',
            'slug'      =>  'text',
            'comment'      =>  'textarea',
            'stock_type'    =>  'text',
            'is_available'    =>  'tel',
            'image'    =>  'file',
        ];
    }

    public function getImageAttribute()
    {
        return '<img src="' . $this->profile_photo_url . '" class="h-8 w-8 full object-cover" />';
    }
}

?>