<?php

namespace Database\Factories;


use App\Models\Component;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Laravel\Jetstream\Features;

class ComponentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Component::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'comment' => $this->faker->sentence(),
            'brand_id' => rand(0, 10),
            'type' => ['Composent_PC','Marques'],
            'serial_number' => Str::random(10),
        ];
    }

}
