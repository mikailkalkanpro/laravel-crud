<?php

namespace Database\Factories;

use App\Models\Computer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Laravel\Jetstream\Features;
use App\Models\Brand;

class ComputerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Computer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $ramOptions = [4, 8, 16, 32, 64];
        $stokageOptions = [1, 2, 3, 4];

        return [
            'name' => $this->faker->word(),
            'slug' => $this->faker->word(),
            'comment' => $this->faker->sentence(),

            'case' => $this->faker->word(),
            'processor' => $this->faker->word(),
            'ram' => $this->faker->randomElement($ramOptions),
            'graphic_card' => $this->faker->word(),
            'stokage' => $this->faker->randomElement($stokageOptions),

            'brand_id' => Brand::factory()->create()->id,
            'is_available' => $this->faker->boolean(),

            'image' => $this->faker->imageUrl(480, 300, 'computers', true),
            'price' => $this->faker->randomFloat(2, 100, 1000, 2000),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

}
