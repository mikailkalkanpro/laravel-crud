<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->dateTime('start_date');
            $table->timestamps();
            $table->tinyInteger('is_accepted');
        });

        
        
        // Schema::table('users', function (Blueprint $table) {
        //     $table->unsignedBigInteger('requests_id')->index()->after('password');
        //     $table->foreign('requests_id')->references('id')->on('requests')->onDelete('cascade');
        // });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
};
