<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lends', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('is_returned');
            $table->string('comment');
            $table->dateTime('ending_at');
            $table->dateTime('returned_at');
            $table->timestamps();
        });

        
        // Schema::table('users', function (Blueprint $table) {
        //     $table->unsignedBigInteger('lend_id')->index()->after('password');
        //     $table->foreign('lend_id')->references('id')->on('lends')->onDelete('cascade');
        // });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lends');
    }
};
