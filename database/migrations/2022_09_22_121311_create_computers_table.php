<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('computers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->text('comment');
            $table->string('case');
            $table->string('processor');
            $table->unsignedTinyInteger('ram');
            $table->string('graphic_card');
            $table->unsignedTinyInteger('stokage');
            $table->boolean('is_available');
            $table->string('image');
            $table->float('price', 8, 2);
            $table->timestamps();
        });

        
        // Schema::table('lends', function (Blueprint $table) {
        //     $table->unsignedBigInteger('computer_id')->index()->after('id');
        //     $table->foreign('computer_id')->references('id')->on('computers')->onDelete('cascade');
        // });

        // Schema::table('requests', function (Blueprint $table) {
        //     $table->unsignedBigInteger('computer_id')->index()->after('id');
        //     $table->foreign('computer_id')->references('id')->on('computers')->onDelete('cascade');
        // });

        // Schema::table('repairs', function (Blueprint $table) {
        //     $table->unsignedBigInteger('computer_id')->index()->after('id');
        //     $table->foreign('computer_id')->references('id')->on('computers')->onDelete('cascade');
        // });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('computers');
    }
};
