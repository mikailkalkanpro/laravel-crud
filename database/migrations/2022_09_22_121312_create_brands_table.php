<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });


        Schema::table('computers', function (Blueprint $table) {
            $table->unsignedBigInteger('brand_id')->index()->after('id')->nullable();

            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
        });

        // Schema::table('components', function (Blueprint $table) {
        //     $table->unsignedBigInteger('brand_id')->index()->after('id')->nullable();

        //     $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
        // });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
    }
};
