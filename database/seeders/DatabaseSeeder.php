<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Brand;
use App\Models\Component;
use App\Models\Computer;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        Role::factory()->create([
            'name' => 'Admin',
            'slug' => 'administrateur',
        ]);

        Role::factory()->create([
            'name' => 'User',
            'slug' => 'utilisateur',
        ]);




        User::factory()->create([
            'name' => 'Test Admin',
            'email' => 'admin@example.com',
            'role_id' => '1',
            'password' => '$2y$10$gTF0fzTSfqIneWRtEUiEnu2dAIYVTvlGOF7TqmD2JgKks/AebJEP2', // adminadmin
        ]);


        User::factory()->create([
            'name' => 'Test User',
            'email' => 'user@example.com',
            'role_id' => '2',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', //password
        ]);


        User::factory(5)->create();

        Computer::factory(10)->create();

        //Brand::factory(10)->create();



    }
}
