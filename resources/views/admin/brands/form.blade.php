<x-app-layout>
    <x-slot name="header">
        <h1 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $brand->id === null ? 'Créer un nouvel élément' : 'Modifier l\'élément' }}
        </h1>
        <nav class="py-3 rounded-md w-full">
            <ol class="list-reset flex">
                <li><a href="{{ route('admin.index') }}" class="text-blue-600 hover:text-blue-700">Accueil</a></li>
                <li><span class="text-gray-500 mx-2">/</span></li>
                <li class="text-gray-500"><a href="{{ route('admin.brands.index') }}" class="text-blue-600 hover:text-blue-700">Liste des brands</a></li>
                <li><span class="text-gray-500 mx-2">/</span></li>
                <li><span class="text-gray-500 mx-2">{{ $brand->id === null ? "Création": 'Modification' }}</span></li>
            </ol>
        </nav>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-form action="{{ $brand->id === null ? route('admin.brands.store') : route('admin.brands.update', $brand->id) }}">
                <x-form-group label="Nom" name="name">
                    <x-input name="name" value="{{ old('name', $brand->name)}}" />
                </x-form-group>

                <x-form-group label="Slug" name="slug">
                    <x-input name="slug" value="{{ old('slug', $brand->slug)}}" />
                </x-form-group>

                <div class="flex space-x-2 justify-center">
                  <button
                    type="button"
                    data-mdb-ripple="true"
                    data-mdb-ripple-color="light"
                    class="inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                  >{{ $brand->id === null ? 'Créer un nouvel élément' : 'Modifier l\'élément' }}</button>
                </div>
            </x-form>
        </div>
    </div>

</x-app-layout>
