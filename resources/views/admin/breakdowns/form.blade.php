<x-app-layout>
    <x-slot name="header">
        <h1 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $breakdown->id === null ? 'Créer un nouvel élément' : 'Modifier l\'élément' }}
        </h1>
        <nav class="py-3 rounded-md w-full">
            <ol class="list-reset flex">
                <li><a href="{{ route('admin.index') }}" class="text-blue-600 hover:text-blue-700">Accueil</a></li>
                <li><span class="text-gray-500 mx-2">/</span></li>
                <li class="text-gray-500"><a href="{{ route('admin.breakdowns.index') }}" class="text-blue-600 hover:text-blue-700">Liste des breakdowns</a></li>
                <li><span class="text-gray-500 mx-2">/</span></li>
                <li><span class="text-gray-500 mx-2">{{ $breakdown->id === null ? "Création": 'Modification' }}</span></li>
            </ol>
        </nav>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-form action="{{ $breakdown->id === null ? route('admin.breakdowns.store') : route('admin.breakdowns.update', $breakdown->id) }}">
                @method($breakdown->id === null ? 'POST' : 'PUT')
                <x-form-group label="Nom" name="name">
                    <x-input name="name" value="{{ old('name', $breakdown->name)}}" />
                </x-form-group>

                <div class="flex space-x-2 justify-center">
                  <button
                    type="button"
                    data-mdb-ripple="true"
                    data-mdb-ripple-color="light"
                    class="inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                  >{{ $breakdown->id === null ? 'Créer un nouvel élément' : 'Modifier l\'élément' }}</button>
                </div>
            </x-form>
        </div>
    </div>

</x-app-layout>
