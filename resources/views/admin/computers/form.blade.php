<x-app-layout>
    <x-slot name="header">
        <h1 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $computer->id === null ? 'Créer un nouvel élément' : 'Modifier l\'élément' }}
        </h1>
        <nav class="py-3 rounded-md w-full">
            <ol class="list-reset flex">
                <li><a href="{{ route('admin.index') }}" class="text-blue-600 hover:text-blue-700">Accueil</a></li>
                <li><span class="text-gray-500 mx-2">/</span></li>
                <li class="text-gray-500"><a href="{{ route('admin.computers.index') }}" class="text-blue-600 hover:text-blue-700">Liste des computers</a></li>
                <li><span class="text-gray-500 mx-2">/</span></li>
                <li><span class="text-gray-500 mx-2">{{ $computer->id === null ? "Création": 'Modification' }}</span></li>
            </ol>
        </nav>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-form action="{{ $computer->id === null ? route('admin.computers.store') : route('admin.computers.update', $computer->id) }}">
                @method($computer->id === null ? 'POST' : 'PUT')
                <x-form-group label="Nom" name="name">
                    <x-input name="name" value="{{ old('name', $computer->name)}}" />
                </x-form-group>

                <x-form-group label="Slug" name="slug">
                    <x-input name="slug"  value="{{ old('slug', $computer->slug) }}" />
                </x-form-group>

                <div class="grid gap-4 grid-cols-3 grid-rows-3">
                    
                    <x-form-group label="Boitier" name="comment">
                        <x-input name="comment"  value="{{ old('comment', $computer->comment) }}" />
                    </x-form-group>

                    <x-form-group label="Processeur" name="stock_type">
                        <x-input name="processeur"  value="{{ old('stock_type', $computer->stock_type) }}" />
                    </x-form-group>

                    <x-form-group label="Ram" name="stock_type">
                        <x-input name="ram"  value="{{ old('stock_type', $computer->stock_type) }}" />
                    </x-form-group>

                    <x-form-group label="Carte Graphique" name="stock_type">
                        <x-input name="stock_type"  value="{{ old('stock_type', $computer->stock_type) }}" />
                    </x-form-group>

                    <x-form-group label="Stokage" name="comment">
                        <x-input name="comment"  value="{{ old('comment', $computer->comment) }}" />
                    </x-form-group>

                    
                    <x-form-group label="Marque PC" name="brand_id">
                        <x-input name="brand"  value="{{ old('brand', $computer->brand) }}" />
                    </x-form-group>
                </div>


                <x-form-group label="Image" name="brand_id">
                    <x-input type="file" name="brand"  value="{{ old('brand', $computer->brand) }}" />
                    
                </x-form-group>

                <x-form-group label="Prix" name="brand_id">
                    <x-input name="brand"  value="{{ old('brand', $computer->brand) }}" />
                </x-form-group>

                <div class="flex space-x-2 justify-center">
                    <button
                    type="button"
                    data-mdb-ripple="true"
                    data-mdb-ripple-color="light"
                    class="inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                    >{{ $computer->id === null ? 'Créer un nouvel élément' : 'Modifier l\'élément' }}</button>
                </div>

            </x-form>
        </div>
    </div>

</x-app-layout>
