<x-app-layout>
    <x-slot name="header">
        <h1 class="font-semibold text-xl text-gray-800 leading-tight">
            Dashboard
        </h1>
        <nav class="py-3 rounded-md w-full">
            <ol class="list-reset flex">
                <li><a href="{{ route('admin.index') }}" class="text-blue-600 hover:text-blue-700">Accueil</a></li>
            </ol>
        </nav>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2>Listes Computers</h2>
            <livewire:product-table buttons-slot="components.add-button" />
        </div>
    </div>



</x-app-layout>
