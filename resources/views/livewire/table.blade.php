<div>
    <table class="min-w-full table-auto">
        <thead>
        <tr>
            @foreach( $fields as $field )
                <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                    {{ __(ucfirst($field)) }}
                </th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach( $items as $item )
        <tr class="border-b">
            @foreach( $fields as $field )
                {{ dump($field) }}
                <td>{!! $item->{$field} !!}</td>
            @endforeach
        </tr>
        @endforeach
        </tbody>
    </table>

</div>
