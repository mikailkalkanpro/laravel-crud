<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\BrandsController;
use App\Http\Controllers\Admin\ComponentsController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\ComputersController;
use App\Http\Controllers\Admin\RolesController;
use App\Http\Controllers\CrudController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

});

Route::prefix('admin')->name('admin.')->middleware(['auth:sanctum', 'admin'])->group(function() {
    Route::get('/', [AdminController::class, 'index'])->name('index');
    Route::get('crud/{model}', [CrudController::class, 'index'])->name('crud.index');
    Route::resource('users', UsersController::class);
    Route::resource('roles', RolesController::class);
    Route::resource('computers', ComputersController::class);
    Route::resource('brands', BrandsController::class);
    Route::resource('components', ComponentsController::class);
    Route::resource('products', ProductController::class);
});




