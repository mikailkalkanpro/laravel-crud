<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testAdminUserExists()
    {
        // Create a role
        $role = Role::factory()->create([
            'name' => 'Admin',
            'slug' => 'administrateur',
        ]);
        dump($role);

        // Create a user and assign the role
        $user = User::factory()->create([
            'role_id' => $role->id,
        ]);
        dump($user);

        // Assert that the user has the assigned role
        $this->assertInstanceOf(Role::class, $user->role);
        $this->assertEquals($role->id, $user->role->id);
    }
}